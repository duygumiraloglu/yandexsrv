﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using yandexsrv.API.Logs;

namespace yandexsrv.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Produces("application/json")]
    public class GeocodeController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<GeocodeController> _logger;

        public GeocodeController(IConfiguration configuration, ILogger<GeocodeController> logger)
        {
            _configuration = configuration;
            _logger = logger;
        }        
                
        [HttpGet(template: "[action]")]
        public async Task<IActionResult> GetParseAddress(string Address)
        {
            Log log = new Log();
            JObject parseAddress = Geocode.GeocodeParse(Address);
            log.Url = "GetParseAddress?Address=" + Address;          
            log.Message = "İstek yapıldı..";
            log.Code = 1;

            if (parseAddress.Count == 0)
            {
                log.Message += "istek sonuç getirmedi..";
                log.Code = -1;
            }           
          
            using (SqlConnection _connection = new SqlConnection(_configuration["ConnectionStrings:Aktif"]))
            {
                try
                {
                     string logResult = LogRepo.AddLog(_connection, log);
                    
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "HATA");                   
                }
            }

            if (parseAddress != null) 
            {
                return Ok(parseAddress.ToString());
            }
            return Ok("Yandex servisinden Adres: '" + Address + "' için bir sonuç getirilemedi..");
           
        }
    }
}
