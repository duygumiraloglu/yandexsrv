﻿using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace yandexsrv.API.Logs
{
    public static class LogRepo
    {
        public static string AddLog(SqlConnection connection, Log log)
        {
            try
            {               
                var p = new DynamicParameters();
                p.Add("@Url", log.Url);
                p.Add("@Message", log.Message);
                p.Add("@Code", log.Code);
            
                return connection.Query("_sp_insert_yandex_service_logs", p, commandType: CommandType.StoredProcedure).FirstOrDefault();

            } 
            catch (Exception ex) 
            {
                return "hata> " + ex.ToString();
            }            
        }
    }
}
