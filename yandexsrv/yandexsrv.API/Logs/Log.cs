﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace yandexsrv.API.Logs
{
    public class Log
    {      
        public string Url { get; set; }
        public string Message { get; set; }
        public int Code { get; set; }        
    }
}
