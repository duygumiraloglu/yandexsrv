﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace yandexsrv.API.Logs
{
    public interface ILog<T> where T : class
    {
        Task<int> AddAsync(T entity);
    }
}
