using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Net;

namespace yandexsrv.API
{
    public class Geocode
    {
        public static JObject GeocodeParse(string address) 
        {
            string apiKey = "c28dacec-b3c8-404f-8032-a02c7e070a4a";
            JObject parseData = new JObject();

            string url = "https://geocode-maps.yandex.ru/1.x/?apikey=" + apiKey + "&geocode=" + address + "&format=json&lang=en_TR";

            WebRequest request = WebRequest.Create(url);
            request.Method = "GET";
            request.Timeout = -1;
            WebResponse response = request.GetResponse();
            Console.WriteLine(((HttpWebResponse)response).StatusDescription);

            using (Stream dataStream = response.GetResponseStream())
            {
                StreamReader reader = new StreamReader(dataStream);
                string responseFromServer = reader.ReadToEnd();
                JObject parseResp = JObject.Parse(responseFromServer);

                JArray resArray = JArray.Parse(parseResp["response"]["GeoObjectCollection"]["featureMember"].ToString());
                if (resArray.Count > 0)
                {
                    parseData = JObject.Parse(resArray[0]["GeoObject"].ToString());
                }
            }
            response.Close();

            return parseData;
        }

    }
}
